#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'ylgrgyq'
SITENAME = 'Blogs by ylgrgyq'
SITEURL = ''
DEFAULT_DATE_FORMAT="%Y-%m-%d"

PATH = 'content'
OUTPUT_PATH = 'public/'

TIMEZONE = 'Asia/Shanghai'

DEFAULT_LANG = 'zh'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_PAGINATION = 20

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

ARTICLE_PATHS = ['articles',]
ARTICLE_URL = '{slug}.html'
ARTICLE_SAVE_AS = '{slug}.html'

THEME="aboutwilson"
STATIC_PATHS = ['images', 'extra']
EXTRA_PATH_METADATA = {
    'extra/favicon.ico': {'path': 'favicon.ico'},
    'extra/robots.txt': {'path': 'robots.txt'},
}

PLUGIN_PATHS = ['./plugins/', ]
PLUGINS=['sitemap',]

SITEMAP = {
    'exclude': ['author/', 'categories\.html', 'tag/', 'archives\.html', 'tags\.html' , 'category/'],
    'format': 'xml',
}

DISQUS_SITENAME = "ylgrgyq-blog"

